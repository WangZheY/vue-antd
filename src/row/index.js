import newButton from './../components/button.vue'
import newTable from './../components/table.vue'
import newPagination  from './../components/pagination.vue'
import newSelect from './../components/select.vue'
import newPicker from './../components/picker.vue'
import newCarousel from './../components/carousel.vue'
import newModal from './../components/modal.vue'
import newSpin from './../components/spin.vue'
import newUpdata from './../components/updata.vue'
import newCheckbox from './../components/checkbox.vue'
import newSelectOption from './../components/selectOption.vue'
import newIcon from './../components/icon.vue'
// 为组件提供 install 方法 用于按需引入
newButton.install = function (Vue) {
    Vue.component(newButton.name, Row)
}
newTable.install = function (Vue) {
    Vue.component(newTable.name, Row)
}
newPagination.install = function (Vue) {
    Vue.component(newPagination.name, Row)
}
newSelect.install = function (Vue) {
    Vue.component(newSelect.name, Row)
}
newPicker.install = function (Vue) {
    Vue.component(newPicker.name, Row)
}
newCarousel.install = function (Vue) {
    Vue.component(newCarousel.name, Row)
}
newModal.install = function (Vue) {
    Vue.component(newModal.name, Row)
}
newSpin.install = function (Vue) {
    Vue.component(newSpin.name, Row)
}
newUpdata.install = function (Vue) {
    Vue.component(newUpdata.name, Row)
}
newCheckbox.install = function (Vue) {
    Vue.component(newCheckbox.name, Row)
}
newSelectOption.install = function (Vue) {
    Vue.component(newSelectOption.name, Row)
}
newIcon.install = function (Vue) {
    Vue.component(newIcon.name, Row)
}

// console.log(newButton,'newButton')
// 导出组件  组件集合，用于遍历
export default [newButton,newTable,newPagination,newSelect,newPicker,newCarousel,newModal,newSpin,newUpdata,newCheckbox,newSelectOption,newIcon]