// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import antd from 'ant-design-vue' // 新增
import 'ant-design-vue/dist/antd.css';

import elementUi from 'element-ui' // 新增
import 'element-ui/lib/theme-chalk/index.css' 
// 导入组件库
import PButton from './packages'
// 使用组件库
Vue.use(PButton)
Vue.config.productionTip = false

Vue.use(elementUi) // 新增
Vue.use(antd)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
